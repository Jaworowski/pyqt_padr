convert UI

C:\Maciej\Python\Python\venv1\Scripts>pyuic5.exe ..\..\pyqt5\pluralsight\my_project\Ui.ui -o ..\..\pyqt5\pluralsight\my_project\Ui.py
pyside2-uic.exe ..\..\pyqt5\pluralsight\my_project\ui\add.ui -o ..\..\pyqt5\pluralsight\my_project\ui\add.py


Spaghetti Napoli

Ingredients:

200g spaghetti
1 onion
2 cloves of garlic
3 tablespoons of olive oil
1 can of chopped tomatoes in tomato juice
herbs (oregano, marjoram, basil)
salt
pepper
parmesan or grated cheese

Preparation:

1. Peel off and chop an onion. Mince garlic with a garlic press.
2. Heat the olive oil in a frying pan. Add chopped onion and minced garlic. Heat everything for 5 minutes.
3. Pour the tomatoes with juice into the frying pan. Stir the sauce for a while. Add herbs, salt and pepper to taste. Cook for 5 minutes. The sauce is ready.
4. In the meantime boil spaghetti as per instruction.
5. When spaghetti is ready, drain it and mix it with the sauce. Put spaghetti to bowls and sprinkle it with grated cheese or parmesan. Enjoy!


pancake

Ingredients

100 grams of flour (mąka)
250 ml of milk (mleko)
butter (masło)
one egg (jajko)
a pinch of salt (szczypta soli)

Kitchen utensils

    a wooden spoon  (drewniana łyżka)
    a bowl (miska)
    a frying pan (patelnia)

Preparation

First, mix the flour, eggs, half the milk and add a pinch of salt. Whisk the ingredients in the bowl and add the rest of the milk. It’s a good idea to leave the mixture for a few minutes. Next, heat up a little butter in the frying pan and pour some mixture.