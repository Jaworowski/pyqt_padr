import sys
from PySide2.QtWidgets import *
from PySide2.QtGui import *
from ui.Ui import Ui_MainWindow
from database.database import Database
from add.add_main import AddWindow
from edit.edit_main import EditWindow


class MainWindow(QDialog, Ui_MainWindow):
    def __init__(self):
        QDialog.__init__(self)
        self.setupUi(self)

        self.database = Database()

        system_icon = QIcon('img/python.png')
        system_notification = QSystemTrayIcon(system_icon, self)

        menu = QMenu()
        minimize = QAction("Minimize", self)
        close = QAction("Close", self)
        maximize = QAction("Maximize", self)

        menu.addActions([minimize, close])
        system_notification.setContextMenu(menu)

        system_notification.show()
        system_notification.showMessage("Maciejs application", "Meal list", system_icon)

        # action for menu system icon
        close.triggered.connect(self.close)
        maximize.triggered.connect(self.showMaximized)
        minimize.triggered.connect(self.showMinimized)

        # menu action
        self.actionExit.triggered.connect(self.close)
        self.actionOpen.triggered.connect(self.file_open_with_database)
        self.actionSave.triggered.connect(self.save_database)
        self.actionNew.triggered.connect(self.new_database)

        # action for push button
        # window add
        self.add_window = AddWindow(self.database)
        self.pushButton_add.clicked.connect(self.show_add_window)

        # Edit window
        self.pushButton_edit.clicked.connect(self.show_edit_window)

        # ListView listViewList_list"
        self.show_menu_list()
        self.listViewList_list.itemClicked.connect(self.menu_recipe_clicked)

    def file_open_with_database(self):
        # get file name from dialog
        file_name = QFileDialog.getOpenFileName(self, 'Open file', r'C:\Maciej\Python\Python\pyqt5\pluralsight',
                                                'DAT(*.dat)')
        file_name = file_name[0]
        if file_name.endswith('.dat'):
            # read database from file
            self.database.open_database(file_name[:-len('.dat')])

    def new_database(self):
        try:
            file_name_tuple = QFileDialog.getSaveFileName(self, 'Backup Contacts',
                                                          r'C:\Maciej\Python\Python\pyqt5\pluralsight',
                                                          'DAT(*.dat)')
            file_name = file_name_tuple[0]
            if file_name.endswith('.dat'):
                self.database.new_database(file_name[:-len('.dat')])

            QMessageBox.information(self, "ListWidget", "You clicked:")

        except Exception as error:
            QMessageBox.information(self, "Error", str(error))

    def save_database(self):
        self.database.write_database()

    def show_add_window(self):
        self.add_window.exec_()

        self.refresh_all_windows()

    def show_edit_window(self):
        try:
            item = self.listViewList_list.currentItem().text()
            edit_window = EditWindow(item, self.database)
            edit_window.exec_()
            self.refresh_all_windows()
        except Exception:
            print("error")
            QMessageBox.information(self, "No item selected", "Please select one!")

    def show_menu_list(self):
        lists = self.database.return_item_keys()
        for item in lists:

            self.listViewList_list.addItem(item)

    def menu_recipe_clicked(self, item):
        components = self.database.return_components(item.text())
        desc = self.database.return_description(item.text())

        self.listView_products.clear()
        self.listView_products.addItems(components)

        self.textBrowser_desc.clear()
        description = ""

        for t in desc:
            description += t + "\n"
        self.textBrowser_desc.setText(description)

    def refresh_all_windows(self):
        self.listViewList_list.clear()
        self.listView_products.clear()
        self.textBrowser_desc.clear()

        self.show_menu_list()

#     TODO add delete button


if __name__ == "__main__":
    app = QApplication(sys.argv)
    dialog = MainWindow()
    dialog.show()
    app.exec_()
