from PySide2.QtWidgets import *
from edit.edit_window import UiEdit


class EditWindow(QDialog, UiEdit):
    def __init__(self, recipe, database):
        QDialog.__init__(self)
        self.setupUi(self)
        self.database = database
        self.recipe_name = recipe

        self.lineEdit_name.setText(recipe)
        self.description.setText(self.get_description(recipe))
        self.products.setPlainText(self.get_ingredients(recipe))

        # Push button event
        self.pushButton_cancel.clicked.connect(self.close)
        self.pushButton_add.clicked.connect(self.update_recipe)

    def get_description(self, product_name):
        """Read description from database
        :param string product_name: product name"""
        desc = self.database.return_description(product_name)

        description_to_print = ""

        for i in desc:
            description_to_print += i
            description_to_print += "\n"

        return description_to_print

    def get_ingredients(self, product_name):
        """Read components from database
        :param string product_name: product name
        """
        components = self.database.return_components(product_name)

        components_to_print = ""

        for i in components:
            components_to_print += i
            components_to_print += "\n"

        return components_to_print

    def update_recipe(self):

        # read current value from fields
        description = self.description.toPlainText()
        components = self.products.toPlainText()
        new_product_name = self.lineEdit_name.text()

        if new_product_name != self.recipe_name:
            self.database.add_recipe(new_product_name, description, components)
            self.database.remove_recipe(self.recipe_name)

            message = "Product was changed"

        else:
            message = "Product was not changed"
            if self.get_description(self.recipe_name) != description:
                self.database.update_recipe(name=new_product_name, desc=description)
            if self.get_ingredients(self.recipe_name) != components:
                self.database.update_recipe(name=new_product_name, components=components)
            else:
                message = "Product was changed"

        QMessageBox.information(self, "OK", message)
        self.close()
