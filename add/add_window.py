# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '..\..\pyqt5\pluralsight\my_project\ui\add.ui',
# licensing of '..\..\pyqt5\pluralsight\my_project\ui\add.ui' applies.
#
# Created: Tue Mar 26 10:12:41 2019
#      by: pyside2-uic  running on PySide2 5.12.2
#
# WARNING! All changes made in this file will be lost!

from PySide2 import QtCore, QtGui, QtWidgets

class Ui_Add(object):
    def setupUi(self, Add):
        Add.setObjectName("Add")
        Add.resize(416, 576)
        Add.setStyleSheet("QWidget {\n"
"background-color: darkgray;\n"
"}\n"
"\n"
"QLabel {\n"
"background-color: darkgray;\n"
"text-align: center;\n"
"}\n"
"\n"
"QListView {\n"
"background-color: lightgray;\n"
"}\n"
"\n"
"QTextBrowser {\n"
"background-color:lightgray;\n"
"}\n"
"\n"
"QPushButton{\n"
"background-color: orange;\n"
"}")
        self.label = QtWidgets.QLabel(Add)
        self.label.setGeometry(QtCore.QRect(10, 60, 55, 16))
        font = QtGui.QFont()
        font.setPointSize(10)
        font.setWeight(75)
        font.setBold(True)

        self.label.setFont(font)
        self.label.setObjectName("label")
        self.label_2 = QtWidgets.QLabel(Add)
        self.label_2.setGeometry(QtCore.QRect(10, 90, 91, 16))

        font = QtGui.QFont()
        font.setPointSize(10)
        self.label_2.setFont(font)
        self.label_2.setObjectName("label_2")
        self.label_3 = QtWidgets.QLabel(Add)
        self.label_3.setGeometry(QtCore.QRect(10, 290, 81, 16))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.label_3.setFont(font)
        self.label_3.setObjectName("label_3")
        self.layoutWidget = QtWidgets.QWidget(Add)
        self.layoutWidget.setGeometry(QtCore.QRect(110, 50, 258, 424))
        self.layoutWidget.setObjectName("layoutWidget")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.layoutWidget)
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout.setObjectName("verticalLayout")
        self.lineEdit_name = QtWidgets.QLineEdit(self.layoutWidget)
        font = QtGui.QFont()
        font.setPointSize(10)
        font.setWeight(75)
        font.setBold(True)
        self.lineEdit_name.setFont(font)
        self.lineEdit_name.setObjectName("lineEdit_name")
        self.verticalLayout.addWidget(self.lineEdit_name)
        self.description = QtWidgets.QTextEdit(self.layoutWidget)
        font = QtGui.QFont()
        font.setPointSize(10)
        self.description.setFont(font)
        self.description.setObjectName("description")
        self.verticalLayout.addWidget(self.description)
        self.products = QtWidgets.QPlainTextEdit(self.layoutWidget)
        font = QtGui.QFont()
        font.setPointSize(10)
        self.products.setFont(font)
        self.products.setObjectName("products")
        self.verticalLayout.addWidget(self.products)
        self.widget = QtWidgets.QWidget(Add)
        self.widget.setGeometry(QtCore.QRect(60, 520, 261, 30))
        self.widget.setObjectName("widget")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.widget)
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.pushButton_add = QtWidgets.QPushButton(self.widget)
        self.pushButton_add.setObjectName("pushButton_add")
        self.horizontalLayout.addWidget(self.pushButton_add)
        self.pushButton_cancel = QtWidgets.QPushButton(self.widget)
        self.pushButton_cancel.setObjectName("pushButton_cancel")
        self.horizontalLayout.addWidget(self.pushButton_cancel)

        self.retranslateUi(Add)
        QtCore.QMetaObject.connectSlotsByName(Add)

    def retranslateUi(self, Add):
        Add.setWindowTitle(QtWidgets.QApplication.translate("Add", "Add new_database item", None, -1))
        self.label.setText(QtWidgets.QApplication.translate("Add", "Name", None, -1))
        self.label_2.setText(QtWidgets.QApplication.translate("Add", "Description", None, -1))
        self.label_3.setText(QtWidgets.QApplication.translate("Add", "Products", None, -1))
        self.pushButton_add.setText(QtWidgets.QApplication.translate("Add", "Add", None, -1))
        self.pushButton_cancel.setText(QtWidgets.QApplication.translate("Add", "Cancel", None, -1))

