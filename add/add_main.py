from PySide2.QtWidgets import *
import time
from add.add_window import Ui_Add


class AddWindow(QDialog, Ui_Add):
    def __init__(self, database):
        QDialog.__init__(self)
        self.setupUi(self)
        self.database = database

        # Push button event
        self.pushButton_cancel.clicked.connect(self.clean_all_fields)
        self.pushButton_add.clicked.connect(self.add_recipe_to_database)

    def add_recipe_to_database(self):
        recipe = self.lineEdit_name.text()
        description = self.description.toPlainText()
        ingredients = self.products.toPlainText()

        if recipe is "" or description is "" or ingredients is "":
            QMessageBox.about(self, "Error", "Please update all fields.")

        else:
            self.database.add_recipe(name=recipe, desc=description, component=ingredients)

            QMessageBox.about(self, "OK", "Recipe was added")

            time.sleep(0.5)

            # clean all fields
            self.lineEdit_name.setText("")
            self.description.setText("")
            self.products.clear()

            self.close()

    def clean_all_fields(self):

        # clean all fields
        self.lineEdit_name.setText("")
        self.description.setText("")
        self.products.clear()

        self.close()
