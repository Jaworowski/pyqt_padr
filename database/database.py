import shelve
import os


class Database(object):
    """Class responsible for database.

    Example of record in database:
    dict = {'name': {'desc': [list],
                     'components': [list]}
    """

    DEFAULT_FILE_NAME = r'database\my_receipts'

    def __init__(self):
        self._db = {}
        self.open_database(os.path.join(os.getcwd(), self.DEFAULT_FILE_NAME))
        self._file_name = self.DEFAULT_FILE_NAME

    def open_database(self, file):
        print(file)
        database_file = shelve.open(file)

        try:
            for k in database_file:
                self._db[k] = database_file[k]

        except Exception as exc:
            print(exc)
            # print("Database don't exist. new database was created.")
            # self._db = {}

        database_file.close()

    def write_database(self, file_name=None):
        if file_name is None:
            file_name = os.path.join(os.getcwd(), self._file_name)
        print(file_name)
        # try:
        #     os.remove(file_name + "backup" + ".dat")
        #     os.rename(file_name + '.dat', file_name + "backup" + ".dat")
        # except Exception as err:
        #     print(err)
        try:
            os.remove(file_name + '.dat')
            os.remove(file_name + '.bak')
            os.remove(file_name + '.dir')
        except Exception as exc:
            print(exc)
        try:
            with shelve.open(file_name) as database_file:
                for k, v in self._db.items():
                    database_file[k] = v
        except Exception as err:
            print(err)

    def convert_string_to_list(self):
        pass

    def add_recipe(self, name, desc, component):
        """ Add new recipe to database

        :param str name: Recipe name
        :param list desc: List of description row
        :param list component: List of components row
        """

        item = {'desc': desc.split('\n'), 'components': component.split('\n')}
        self._db[name] = item

        print(self._db)

    def new_database(self, file_name):
        print(file_name)
        self._db = {}
        self._file_name = file_name

    def return_item_keys(self):
        return self._db.keys()

    def return_components(self, key):
        return self._db[key]['components']

    def return_description(self, key):
        return self._db[key]['desc']

    def remove_recipe(self, name):
        self._db.pop(name)

    def update_recipe(self, name, desc=None, components=None):
        if desc is not None:
            self._db[name].update(desc=desc.split('\n'))

        if components is not None:
            self._db[name].update(components=components.split('\n'))
