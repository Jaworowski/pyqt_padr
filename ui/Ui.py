from PySide2 import QtCore, QtGui, QtWidgets


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(1040, 750)
        MainWindow.setMinimumSize(QtCore.QSize(1040, 750))
        MainWindow.setStyleSheet("QWidget {\n"
                                 "background-color: darkgray;\n"
                                 "}\n"
                                 "\n"
                                 "QLabel {\n"
                                 "background-color: darkgray;\n"
                                 "text-align: center;\n"
                                 "}\n"
                                 "\n"
                                 "QListView {\n"
                                 "background-color: lightgray;\n"
                                 "}\n"
                                 "\n"
                                 "QTextBrowser {\n"
                                 "background-color:lightgray;\n"
                                 "}\n"
                                 "\n"
                                 "QPushButton{\n"
                                 "background-color: orange;\n"
                                 "}")
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setStyleSheet("")
        self.centralwidget.setObjectName("centralwidget")

        font = QtGui.QFont()
        font.setPointSize(12)
        font.setBold(True)
        font.setWeight(75)

        self.listViewList_list = QtWidgets.QListWidget(self.centralwidget)
        self.listViewList_list.setGeometry(QtCore.QRect(10, 50, 541, 561))
        self.listViewList_list.setStyleSheet("")
        self.listViewList_list.setObjectName("listViewList_list")
        self.listViewList_list.setFont(font)

        font.setBold(False)
        self.listView_products = QtWidgets.QListWidget(self.centralwidget)
        self.listView_products.setGeometry(QtCore.QRect(560, 50, 461, 291))
        self.listView_products.setFont(font)
        self.listView_products.setStyleSheet("")
        self.listView_products.setObjectName("listView_products")

        self.textBrowser_desc = QtWidgets.QTextBrowser(self.centralwidget)
        self.textBrowser_desc.setGeometry(QtCore.QRect(560, 380, 461, 311))
        self.textBrowser_desc.setStyleSheet("")
        self.textBrowser_desc.setObjectName("textBrowser_desc")
        self.textBrowser_desc.setFont(font)

        self.pushButton_add = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_add.setGeometry(QtCore.QRect(120, 630, 131, 61))
        font = QtGui.QFont()
        font.setPointSize(12)
        font.setBold(True)
        font.setWeight(75)
        self.pushButton_add.setFont(font)
        self.pushButton_add.setStyleSheet("")
        self.pushButton_add.setObjectName("pushButton_add")
        self.pushButton_edit = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_edit.setGeometry(QtCore.QRect(280, 630, 131, 61))
        font = QtGui.QFont()
        font.setPointSize(12)
        font.setBold(True)
        font.setWeight(75)
        self.pushButton_edit.setFont(font)
        self.pushButton_edit.setStyleSheet("")
        self.pushButton_edit.setObjectName("pushButton_edit")

        self.label_list = QtWidgets.QLabel(self.centralwidget)
        self.label_list.setGeometry(QtCore.QRect(20, 25, 531, 21))
        font = QtGui.QFont()
        font.setPointSize(10)
        font.setBold(True)
        font.setWeight(75)
        self.label_list.setFont(font)
        self.label_list.setAlignment(QtCore.Qt.AlignCenter)
        self.label_list.setObjectName("label_list")

        self.label_products = QtWidgets.QLabel(self.centralwidget)
        self.label_products.setGeometry(QtCore.QRect(570, 25, 451, 21))

        font = QtGui.QFont()
        font.setPointSize(10)
        font.setBold(True)
        font.setWeight(75)

        self.label_products.setFont(font)
        self.label_products.setAlignment(QtCore.Qt.AlignCenter)
        self.label_products.setObjectName("label_products")
        self.label_desc = QtWidgets.QLabel(self.centralwidget)
        self.label_desc.setGeometry(QtCore.QRect(570, 355, 451, 21))

        font = QtGui.QFont()
        font.setPointSize(10)
        font.setBold(True)
        font.setWeight(75)

        self.label_desc.setFont(font)
        self.label_desc.setAlignment(QtCore.Qt.AlignCenter)
        self.label_desc.setObjectName("label_desc")

        # MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 1040, 26))
        self.menubar.setObjectName("menubar")
        self.menuFile = QtWidgets.QMenu(self.menubar)
        self.menuFile.setObjectName("menuFile")
        # MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        # MainWindow.setStatusBar(self.statusbar)
        self.actionNew = QtWidgets.QAction(MainWindow)
        self.actionNew.setObjectName("actionNew")
        self.actionSave = QtWidgets.QAction(MainWindow)
        self.actionSave.setObjectName("actionSave")
        self.actionOpen = QtWidgets.QAction(MainWindow)
        self.actionOpen.setObjectName("actionOpen")
        self.actionExit = QtWidgets.QAction(MainWindow)
        self.actionExit.setObjectName("actionExit")
        self.menuFile.addAction(self.actionNew)
        self.menuFile.addAction(self.actionSave)
        self.menuFile.addAction(self.actionOpen)
        self.menuFile.addAction(self.actionExit)
        self.menubar.addAction(self.menuFile.menuAction())

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.pushButton_add.setText(_translate("MainWindow", "Add"))
        self.pushButton_edit.setText(_translate("MainWindow", "Edit"))
        self.label_list.setText(_translate("MainWindow", "List"))
        self.label_products.setText(_translate("MainWindow", "Products"))
        self.label_desc.setText(_translate("MainWindow", "Description"))
        self.menuFile.setTitle(_translate("MainWindow", "File"))

        self.actionNew.setText(_translate("MainWindow", "New"))
        self.actionNew.setShortcut(_translate("MainWindow", "Ctrl+N"))
        self.actionSave.setText(_translate("MainWindow", "Save"))
        self.actionSave.setShortcut(_translate("MainWindow", "Ctrl+S"))
        self.actionOpen.setText(_translate("MainWindow", "Open"))
        self.actionOpen.setShortcut(_translate("MainWindow", "Ctrl+O"))
        self.actionExit.setText(_translate("MainWindow", "Exit"))
        self.actionExit.setShortcut(_translate("MainWindow", "Ctrl+Q"))


